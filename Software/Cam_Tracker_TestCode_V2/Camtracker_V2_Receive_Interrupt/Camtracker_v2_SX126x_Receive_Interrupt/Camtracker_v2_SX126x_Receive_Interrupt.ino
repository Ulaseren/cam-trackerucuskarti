

// include the library
#include <RadioLib.h>
#include "SPI.h"
SPIClass SPI2(HSPI);

// SX1262 has the following connections:
// NSS pin:   15
// DIO1 pin:  4
// NRST pin:  32
// BUSY pin:  33
SX1262 radio = new Module(15, 4, 32, 33,SPI2);



float ByteToFloat(byte *byterray,int ca)
{
  float f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

  long ByteTolong(byte *byterray,int ca)
{
  long f;
  ((uint8_t*)&f)[0]=byterray[ca];
  ((uint8_t*)&f)[1]=byterray[ca+1];
  ((uint8_t*)&f)[2]=byterray[ca+2];
  ((uint8_t*)&f)[3]=byterray[ca+3];
return f;
  }

float bmp280_data[3];
float mcp_data[1];
long gps_data[3];
byte SIV;
float battery;
uint32_t tt=0;

float aa;
long l;
int ii,c,m;




int transmissionState = ERR_NONE;




String All_data1;

struct Data{
byte byteArray[100];
int counter=0;
};
Data data;

  
void setup() {
  const int HSPI_MISO = 12;
  const int HSPI_MOSI = 13;
  const int HSPI_SCK = 14;  
  const int HSPI_CS = 15;

  SPI2.begin(HSPI_SCK,HSPI_MISO,HSPI_MOSI,HSPI_CS); 
  
  delay(1000);
  Serial.begin(115200);

  // initialize SX1262 with default settings
  Serial.print(F("[SX1262] Initializing ... "));
  int state = radio.begin();
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true);
  }

  // set the function that will be called
  // when new packet is received
  radio.setDio1Action(setFlag);

  // start listening for LoRa packets
  Serial.print(F("[SX1262] Starting to listen ... "));
  state = radio.startReceive();
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true);
  }


}

// flag to indicate that a packet was received
volatile bool receivedFlag = false;

// disable interrupt when it's not needed
volatile bool enableInterrupt = true;

// this function is called when a complete packet
// is received by the module
// IMPORTANT: this function MUST be 'void' type
//            and MUST NOT have any arguments!
void setFlag(void) {
  // check if the interrupt is enabled
  if(!enableInterrupt) {
    return;
  }

  // we got a packet, set the flag
  receivedFlag = true;
}

void loop() {

  
  if(receivedFlag) {


    enableInterrupt = false;
   
    receivedFlag = false;
   
    int state = radio.readData(data.byteArray,100);
   

    if (state == ERR_NONE) {

      Serial.println(F("[SX1262] Received packet!"));

      c=0;
for( ii=0;ii<1;ii++){ 
mcp_data[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}
for( ii=0;ii<3;ii++){ 
bmp280_data[ii]= ByteToFloat(data.byteArray,c);
c+=4;
}


for( ii=0;ii<3;ii++){ 
gps_data[ii]= ByteTolong(data.byteArray,c);
c+=4;
}

SIV=data.byteArray[c];
c+=1;



c=0;

All_data1=String(mcp_data[0])+",";
    All_data1+=String(bmp280_data[0])+","+String(bmp280_data[1])+","+String(bmp280_data[2])+",";
    All_data1+=String(gps_data[0])+","+String(gps_data[1])+","+String(gps_data[2])+",";
    All_data1+=String(SIV)+","+ "\n";
 


      Serial.print(All_data1); Serial.print(F(", "));
     
      Serial.println("");
  
      // print RSSI (Received Signal Strength Indicator)
      Serial.print(F("[SX1262] RSSI:\t\t"));
      Serial.print(radio.getRSSI());
      Serial.println(F(" dBm"));

      // print SNR (Signal-to-Noise Ratio)
      Serial.print(F("[SX1262] SNR:\t\t"));
      Serial.print(radio.getSNR());
      Serial.println(F(" dB"));} 

     else if (state == ERR_CRC_MISMATCH) {
      // packet was received, but is malformed
      Serial.println(F("CRC error!"));

    } else {
      // some other error occurred
      Serial.print(F("failed, code "));
      Serial.println(state);

    }  

    // put module back to listen mode
    radio.startReceive();

    // we're ready to receive more packets,
    // enable interrupt service routine
    enableInterrupt = true;}
    
  }
