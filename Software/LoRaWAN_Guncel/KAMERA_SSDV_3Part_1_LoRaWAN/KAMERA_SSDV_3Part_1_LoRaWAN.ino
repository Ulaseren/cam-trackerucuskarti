#include <Arduino.h>
#include <esp_camera.h>
#include "ssdv.h"
#include "PCA9539.h"
#include "driver/rtc_io.h"
#include "soc/soc.h" //disable brownout problems
#include "soc/rtc_cntl_reg.h" //disable brownout problems

#include <SX126x-Arduino.h>
#include <LoRaWan-Arduino.h>
#include <SPI.h>
#include <Wire.h>

#define LORAWAN_APP_DATA_BUFF_SIZE 89  /**< Size of the data to be transmitted. */
#define LORAWAN_APP_TX_DUTYCYCLE 30000 /**< Defines the application data transmission duty cycle. 30s, value in [ms]. */
#define APP_TX_DUTYCYCLE_RND 1000   /**< Defines a random delay for application data transmission duty cycle. 1s, value in [ms]. */
#define JOINREQ_NBTRIALS 3         /**< Number of trials for the join request. */


#define LORAWAN_DEFAULT_DATARATE DR_3         /**          /LoRaMac datarates definition, from DR_0 to DR_5/ */
#define LORAWAN_DEFAULT_TX_POWER TX_POWER_0  /**   /LoRaMac tx power definition, from TX_POWER_0 to TX_POWER_15/ */



int capture_interval = 30000; // microseconds between captures
long current_millis;
long last_capture_millis = 0;
static esp_err_t cam_err;

int t,n,w=0;

//int counter = 0;

// CAMERA_MODEL_AI_THINKER
#define PWDN_GPIO_NUM     -1
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27

#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22

// ssdv definitions
#define IMG_BUFF_SIZE               128         // size of the buffer feeding SSDV process
#define LORA_BUFFER                 256
#define GPS_BUFFER                  256

uint8_t ssdvQuality = 4;                        // 0-7 corresponding to JPEG quality: 13, 18, 29, 43, 50, 71, 86 and 100


static const char callsign[] = "TA2NHP";        // maximum of 6 characters
ssdv_t ssdv;
uint8_t imgBuff[IMG_BUFF_SIZE];
uint8_t loraBuff[LORA_BUFFER];
char gpsMessage[GPS_BUFFER];
char tempStr[40];
int imageID = 0;
int p=0;




TaskHandle_t Task1; //For Dual Core System

DeviceClass_t CurrentClass = CLASS_A;          /* Class definition*/
LoRaMacRegion_t CurrentRegion = LORAMAC_REGION_EU868;    /* Region:EU868*/
lmh_confirm CurrentConfirm = LMH_UNCONFIRMED_MSG;  

hw_config hwConfig;

// ESP32 - SX126x pin configuration
int PIN_LORA_RESET = 32;   // LORA RESET
int PIN_LORA_NSS = 15;   // LORA SPI CS
int PIN_LORA_SCLK = 14;  // LORA SPI CLK
int PIN_LORA_MISO = 12;  // LORA SPI MISO
int PIN_LORA_DIO_1 = 4; // LORA DIO_1
int PIN_LORA_BUSY = 33;  // LORA SPI BUSY
int PIN_LORA_MOSI = 13;  // LORA SPI MOSI
int RADIO_TXEN = -1;   //2 LORA ANTENNA TX ENABLE ,issues kısmında bu txen ve rxen kesinlikle tanımlı olması gerektiğini söylemişler yoksa asla başarılı olamayacağını bildirmiişler.
int RADIO_RXEN = -1;   //25 LORA ANTENNA RX ENABLE ,Anten anahtarını kontrol etmeden gönderebilirsiniz,Join Accept  paketini asla alamazsınız.




uint8_t nodeDeviceEUI[8] = {0x60, 0x81, 0xF9, 0x18, 0x5A, 0x36, 0xEF, 0xD7}; // msb

uint8_t nodeAppEUI[8] = {0x60, 0x81, 0xF9, 0x6E, 0x4B, 0xF1, 0x18, 0xD8}; // msb

uint8_t nodeAppKey[16] = {0xE7, 0xE7, 0x39, 0xAC, 0x01, 0xE2, 0x2A, 0x10, 0x92, 0x3F, 0x56, 0xA4, 0x6E, 0x5D, 0x3A, 0x2E}; //msb



// Foward declaration
/** LoRaWAN callback when join network finished */
static void lorawan_has_joined_handler(void);
/** LoRaWAN callback when join network failed */
static void lorawan_join_fail_handler(void);
/** LoRaWAN callback when data arrived */
static void lorawan_rx_handler(lmh_app_data_t *app_data);
/** LoRaWAN callback after class change request finished */
static void lorawan_confirm_class_handler(DeviceClass_t Class);
/** LoRaWAN callback after class change request finished */
static void lorawan_unconfirm_tx_finished(void);
/** LoRaWAN callback after class change request finished */
static void lorawan_confirm_tx_finished(bool result);
/** LoRaWAN Function to send a package */
static void send_lora_frame(void);
void save_photo(void);
static uint32_t timers_init(void);

void setupCamera(void);


// APP_TIMER_DEF(lora_tx_timer_id);                                              ///< LoRa tranfer timer instance.
 TimerEvent_t appTimer;                              ///< LoRa tranfer timer instance.
static uint8_t m_lora_app_data_buffer[LORAWAN_APP_DATA_BUFF_SIZE];        ///< Lora user application data buffer.
static lmh_app_data_t m_lora_app_data = {m_lora_app_data_buffer, 0, 0, 0, 0}; ///< Lora user application data structure.

/**@brief Structure containing LoRaWan parameters, needed for lmh_init()
 */
static lmh_param_t lora_param_init = {LORAWAN_ADR_OFF,LORAWAN_DEFAULT_DATARATE, LORAWAN_PUBLIC_NETWORK, JOINREQ_NBTRIALS, LORAWAN_DEFAULT_TX_POWER};
// DR_8
// LORAWAN_DEFAULT_DATARATE
/**@brief Structure containing LoRaWan callback functions, needed for lmh_init()
*/
static lmh_callback_t lora_callbacks = {BoardGetBatteryLevel, BoardGetUniqueId, BoardGetRandomSeed,
                    lorawan_rx_handler, lorawan_has_joined_handler, lorawan_confirm_class_handler, lorawan_join_fail_handler,
                    lorawan_unconfirm_tx_finished, lorawan_confirm_tx_finished};
                   

// Initialize Scheduler and timer
uint32_t err_code = timers_init();
boolean CameraSetup=false; //do not change this.


PCA9539 ioport(0x74);

void setup() {
  delay(5000);
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0);

  Serial.begin(115200);
  Serial.println("=====================================");
  Serial.println("SX126x LoRaWan test");
  Serial.println("=====================================");
 
  Wire.begin(26,27);
  ioport.pinMode(pb2, OUTPUT);
  ioport.digitalWrite(pb2, LOW); // Kamera Aktif.
  delay(100);

  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;
// 0-8 corresponfing to 320x240, 352x288, 640x480, 800x480, 1024x768, 1280x960, 1600x1200, 2048x1536, 2592x1944
//  FRAMESIZE_QVGA 320x240, FRAMESIZE_VGA 640x480, FRAMESIZE_SVGA 800x600
//  FRAMESIZE_XGA 1024x768, FRAMESIZE_UXGA 1600x1200, FRAMESIZE_QXGA 2048x1536
  config.frame_size = FRAMESIZE_VGA;
  config.jpeg_quality = 10;
  config.fb_count = 1;

 
  // camera init
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x",err);
    return;
  }else Serial.println("Camera..OK");


  delay(2000);//kamera beyaz dengesi vb. ayarları yapmak için zamana ihtiyaç duyuyor
 

 hwConfig.CHIP_TYPE = SX1262_CHIP;     // Example uses an eByte E22 module with an SX1262
  hwConfig.PIN_LORA_RESET = PIN_LORA_RESET; // LORA RESET
  hwConfig.PIN_LORA_NSS = PIN_LORA_NSS;  // LORA SPI CS
  hwConfig.PIN_LORA_SCLK = PIN_LORA_SCLK;   // LORA SPI CLK
  hwConfig.PIN_LORA_MISO = PIN_LORA_MISO;   // LORA SPI MISO
  hwConfig.PIN_LORA_DIO_1 = PIN_LORA_DIO_1; // LORA DIO_1
  hwConfig.PIN_LORA_BUSY = PIN_LORA_BUSY;   // LORA SPI BUSY
  hwConfig.PIN_LORA_MOSI = PIN_LORA_MOSI;   // LORA SPI MOSI
  hwConfig.RADIO_TXEN = -1;         // LORA ANTENNA TX ENABLE
  hwConfig.RADIO_RXEN = -1;         // LORA ANTENNA RX ENABLE
  hwConfig.USE_DIO2_ANT_SWITCH = true;    // Example uses an CircuitRocks Alora RFM1262 which uses DIO2 pins as antenna control
  hwConfig.USE_DIO3_TCXO = true;        // Example uses an CircuitRocks Alora RFM1262 which uses DIO3 to control oscillator voltage
  hwConfig.USE_DIO3_ANT_SWITCH = false;  // Only Insight ISP4520 module uses DIO3 as antenna control


  if (err_code != 0)
  {
    Serial.printf("timers_init failed - %d\n", err_code);
  }

  // Initialize LoRa chip.
  err_code = lora_hardware_init(hwConfig);
  if (err_code != 0)
  {
    Serial.printf("lora_hardware_init failed - %d\n", err_code);
  }

  // Setup the EUIs and Keys
  lmh_setDevEui(nodeDeviceEUI);
  lmh_setAppEui(nodeAppEUI);
  lmh_setAppKey(nodeAppKey); // Only needed for OOTA registration

  xTaskCreatePinnedToCore( // For Dual Core System
                    Task1code,   /* Task function. */
                    "Task1",     /* name of task. */
                    10000,       /* Stack size of task */
                    NULL,        /* parameter of the task */
                    1,           /* priority of the task */
                    &Task1,      /* Task handle to keep track of created task */
                    0);          /* pin task to core 0 */                  
  delay(100);
 
 
}

/**@brief Main loop
 */

void Task1code( void * pvParameters ){
 // Serial.print("Task1 running on core ");
 // Serial.println(xPortGetCoreID());
  // Define the HW configuration between MCU and SX126x
 

 
  for(;;){ // We can use inside for loop like void loop if you use this , code will be run core 0
    vTaskDelay(3);

 
  if(w==0){  
  Serial.println(F("lorawan init........"));

    // Initialize LoRaWan
  err_code = lmh_init(&lora_callbacks, lora_param_init, true, CurrentClass,CurrentRegion);
  if (err_code != 0)
  {
    Serial.printf("lmh_init failed - %d\n", err_code);
  }

  // For some regions we might need to define the sub band the gateway is listening to
  /// \todo This is for Dragino LPS8 gateway. How about other gateways???
  if (!lmh_setSubBandChannels(1))
  {
    Serial.println("lmh_setSubBandChannels failed. Wrong sub band requested?");
  }

  lmh_join();
 
  w=1;
     

  }    
}}

void loop()
{
  delay(100);
  /*
  current_millis = millis();
  if (current_millis - last_capture_millis > capture_interval) { // Take another picture
   
    save_photo();
    last_capture_millis = millis();
    //imageID++;
  }
  */
  if(lmh_join_status_get() == LMH_SET){
 
  if(p==0){
  save_photo();
  p=1;}
}


}



int iread(uint8_t *buffer,int numBytes,camera_fb_t *fb, int fbIndex ){

  int bufSize = 0;
  // have we reached past end of imagebuffer
  if((fbIndex + numBytes ) < fb->len){
 
    bufSize = numBytes;
  }
  else{

    bufSize = fb->len - fbIndex;
  }
  // clear the dest buffer
  memset(buffer,0,numBytes);
  memcpy(buffer,&fb->buf[fbIndex],bufSize);
  return bufSize;
}



char Hex(uint8_t index)
  {
    char HexTable[] = "0123456789ABCDEF";
   
    return HexTable[index];
  }

int process_ssdv(camera_fb_t *fb){

  int index=0,c = 0,ssdvPacketCount=0;

  // initialise ssdv config structure
  ssdv_enc_init(&ssdv, SSDV_TYPE_NOFEC, (char *)callsign, imageID++, ssdvQuality);
  // set the output lora packet buffer for ssdv where the final ssdv packet will end up
  ssdv_enc_set_buffer(&ssdv, loraBuff);
  Serial.print("Sending Image: length = ");
  Serial.println(fb->len);

  while(1){
 
    while((c = ssdv_enc_get_packet(&ssdv)) == SSDV_FEED_ME)
    {
        index += iread(imgBuff, IMG_BUFF_SIZE,fb,index);
        Serial.print("index = ");
        Serial.print(index);
        ssdv_enc_feed(&ssdv, imgBuff, IMG_BUFF_SIZE);
    }
   
    if(c == SSDV_EOI)
    {
        Serial.println("ssdv EOI");
        break;
    }
    else if(c != SSDV_OK)
    {
        Serial.println("ssdv Error");
        break;
    }

    // move lora data backwrds 1 byte This seems needed for pits gateway to read it. (TT7)
    for(uint16_t i = 0; i < 256; i++) {// 256
      loraBuff[i] = loraBuff[i+1];
    }

   
    // lora transmit
    Serial.print(" packet sent");
    Serial.println(ssdvPacketCount);
//    uint8_t partBuff[90];


  for(int a=0; a<3;a++){
    
    memset(m_lora_app_data.buffer, 0, 90);//buffer temizle

    m_lora_app_data.buffer[0]= 0x55; // google sheet tarafında ilk 4 byte kontrol ederiz doğru şekilde geliyorsa diğer kısıları yaparız.
    m_lora_app_data.buffer[1]= (uint8_t) imageID;
    m_lora_app_data.buffer[2]= (uint8_t) ssdvPacketCount;
    m_lora_app_data.buffer[3]=  a;
   
   

    for(int z=0;z<85;z++) m_lora_app_data.buffer[(4+z)]=loraBuff[z+(a*85)];  

       

    m_lora_app_data.buffsize =89; //89
    
    delay(10000);
    send_lora_frame();  

    
      
  }  
 
  ssdvPacketCount++;
  delay(1000);  
} }  

void save_photo()
{

  Serial.print("Taking picture: ");
  camera_fb_t *fb = esp_camera_fb_get();
  //delay(10);
  //fb = esp_camera_fb_get();
  process_ssdv(fb);
  esp_camera_fb_return(fb);
}

static void lorawan_join_fail_handler(void)
{
  Serial.println("OTAA joined failed");
  Serial.println("Check LPWAN credentials and if a gateway is in range");
  // Restart Join procedure
  Serial.println("Restart network join request");
}

/**@brief LoRa function for handling HasJoined event.
 */
static void lorawan_has_joined_handler(void)
{
#if (OVER_THE_AIR_ACTIVATION != 0)
  Serial.println("Network Joined");
#else
  Serial.println("OVER_THE_AIR_ACTIVATION != 0");

#endif
  lmh_class_request(CurrentClass);

  TimerSetValue(&appTimer, LORAWAN_APP_TX_DUTYCYCLE);
  TimerStart(&appTimer);
  // app_timer_start(lora_tx_timer_id, APP_TIMER_TICKS(LORAWAN_APP_TX_DUTYCYCLE), NULL);
  Serial.println("Sending frame");
  send_lora_frame();
}

/**@brief Function for handling LoRaWan received data from Gateway
 *
 * @param[in] app_data  Pointer to rx data
 */
static void lorawan_rx_handler(lmh_app_data_t *app_data)
{
  Serial.printf("LoRa Packet received on port %d, size:%d, rssi:%d, snr:%d\n",
          app_data->port, app_data->buffsize, app_data->rssi, app_data->snr);

  for (int i = 0; i < app_data->buffsize; i++)
  {
    Serial.printf("%0X ", app_data->buffer[i]);
  }
  Serial.println("");

  switch (app_data->port)
  {
  case 3:
    // Port 3 switches the class
    if (app_data->buffsize == 1)
    {
      switch (app_data->buffer[0])
      {
      case 0:
        lmh_class_request(CLASS_A);
        break;

      case 1:
        lmh_class_request(CLASS_B);
        break;

      case 2:
        lmh_class_request(CLASS_C);
        break;

      default:
        break;
      }
    }
    break;

  case LORAWAN_APP_PORT:
    // YOUR_JOB: Take action on received data
    break;

  default:
    break;
  }
}

/**@brief Function to confirm LORaWan class switch.
 *
 * @param[in] Class  New device class
 */
static void lorawan_confirm_class_handler(DeviceClass_t Class)
{
  Serial.printf("switch to class %c done\n", "ABC"[Class]);

  // Informs the server that switch has occurred ASAP
  m_lora_app_data.buffsize = 0;
  m_lora_app_data.port = LORAWAN_APP_PORT;
  lmh_send(&m_lora_app_data, CurrentConfirm);
}

/**
 * @brief Called after unconfirmed packet was sent
 *
 */
static void lorawan_unconfirm_tx_finished(void)
{
  Serial.println("Uncomfirmed TX finished");
}

/**
 * @brief Called after confirmed packet was sent
 *
 * @param result Result of sending true = ACK received false = No ACK
 */
static void lorawan_confirm_tx_finished(bool result)
{
  Serial.printf("Comfirmed TX finished with result %s", result ? "ACK" : "NAK");
}

/**@brief Function for sending a LoRa package.
 */
static void send_lora_frame(void)
{
  if (lmh_join_status_get() != LMH_SET)
  {
    //Not joined, try again later
    Serial.println("Did not join network, skip sending frame");
    return;
  }

  m_lora_app_data.port = LORAWAN_APP_PORT;


  Serial.print("Data: ");
   for(int i=0; i<89; i++){
    Serial.print("0x"); if (abs(m_lora_app_data.buffer[i]) < 16) Serial.print("0");
    Serial.print(m_lora_app_data.buffer[i],HEX);
    Serial.print(",");
    }
  Serial.println();
  Serial.print("Size: ");
  Serial.println(m_lora_app_data.buffsize);
  Serial.print("Port: ");
  Serial.println(m_lora_app_data.port);

 
  lmh_error_status error = lmh_send(&m_lora_app_data, CurrentConfirm);
  if (error == LMH_SUCCESS)
  {
  }
  Serial.printf("lmh_send result %d\n", error);

}

 

/**@brief Function for handling a LoRa tx timer timeout event.
 */
 static void tx_lora_periodic_handler(void)
{/*
  TimerSetValue(&appTimer, LORAWAN_APP_TX_DUTYCYCLE);
  TimerStart(&appTimer);
  Serial.println("Sending frame");
  send_lora_frame();  */
}

/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates and starts application timers.
 */
static uint32_t timers_init(void)
{ /*
  appTimer.timerNum = 3;
  TimerInit(&appTimer, tx_lora_periodic_handler);
  return 0;  */  
}
