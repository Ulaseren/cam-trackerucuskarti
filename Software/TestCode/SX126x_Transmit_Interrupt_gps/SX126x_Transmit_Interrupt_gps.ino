/*
   RadioLib SX126x Transmit with Interrupts Example

   This example transmits LoRa packets with one second delays
   between them. Each packet contains up to 256 bytes
   of data, in the form of:
    - Arduino String
    - null-terminated char array (C-string)
    - arbitrary binary data (byte array)

   Other modules from SX126x family can also be used.

   For default module settings, see the wiki page
   https://github.com/jgromes/RadioLib/wiki/Default-configuration#sx126x---lora-modem

   For full API reference, see the GitHub Pages
   https://jgromes.github.io/RadioLib/
*/

// include the library
#include <RadioLib.h>
#include "SPI.h"
SPIClass SPI2(HSPI);
#include <Wire.h> //Needed for I2C to GNSS
#include <SparkFun_u-blox_GNSS_Arduino_Library.h>
SFE_UBLOX_GNSS myGNSS; 
// SX1262 has the following connections:
// NSS pin:   15
// DIO1 pin:  4
// NRST pin:  32
// BUSY pin:  33
SX1262 radio = new Module(15, 4, 32, 33,SPI2);
long lastTime = 0;
// or using RadioShield
// https://github.com/jgromes/RadioShield
//SX1262 radio = RadioShield.ModuleA;

// save transmission state between loops
int transmissionState = ERR_NONE;
void floatAddByte(float data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}

void longAddByte(long data,int *count,byte *array){
  for(int i=0;i<4;i++){
    array[*count]=((uint8_t*)&data)[i];
    (*count)++;
  }
}
void byteAddByte(byte data,int *count,byte *array){
    array[*count]=data;
    (*count)++;
}
struct Data{
byte byteArray[100];
int counter=0;
};
Data data;
void setup() {
  const int HSPI_MISO = 12;
  const int HSPI_MOSI = 13;
  const int HSPI_SCK = 14;  
  const int HSPI_CS = 15;

  SPI2.begin(HSPI_SCK,HSPI_MISO,HSPI_MOSI,HSPI_CS); 
  
  delay(1000);
  Serial.begin(115200);
  Wire.begin(26,27);
  if (myGNSS.begin() == false) //Connect to the u-blox module using Wire port
  {
    Serial.println(F("u-blox GNSS not detected at default I2C address. Please check wiring. Freezing."));
    while (1)
      ;
  }

  myGNSS.setI2COutput(COM_TYPE_UBX); //Set the I2C port to output UBX only (turn off NMEA noise)
  //myGNSS.saveConfiguration();        //Optional: Save the current settings to flash and BBR
  // initialize SX1262 with default settings
  Serial.print(F("[SX1262] Initializing ... "));
  int state = radio.begin();
  if (state == ERR_NONE) {
    Serial.println(F("success!"));
  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true);
  }

  // set the function that will be called
  // when packet transmission is finished
  radio.setDio1Action(setFlag);

  // start transmitting the first packet
  Serial.print(F("[SX1262] Sending first packet ... "));

  floatAddByte(0.49,&data.counter,data.byteArray);
  floatAddByte(-0.49,&data.counter,data.byteArray);
  floatAddByte(1.22,&data.counter,data.byteArray);
  floatAddByte(36.52,&data.counter,data.byteArray);
  floatAddByte(996.33,&data.counter,data.byteArray);
  floatAddByte(-0.46,&data.counter,data.byteArray);
  floatAddByte(-0.14,&data.counter,data.byteArray);
  floatAddByte(-9.66,&data.counter,data.byteArray);
  floatAddByte(-0.00,&data.counter,data.byteArray);
  floatAddByte(0.00,&data.counter,data.byteArray);
  floatAddByte(0.00,&data.counter,data.byteArray);
  floatAddByte(-25.69,&data.counter,data.byteArray);
  floatAddByte(-35.19,&data.counter,data.byteArray);
  floatAddByte(12.44,&data.counter,data.byteArray);
  floatAddByte(-179.00,&data.counter,data.byteArray);
  floatAddByte(2.93,&data.counter,data.byteArray);
  floatAddByte(-33.18,&data.counter,data.byteArray);

  longAddByte(-118334965,&data.counter,data.byteArray);
  longAddByte(95731051,&data.counter,data.byteArray);
  longAddByte(892087,&data.counter,data.byteArray);
  byteAddByte(0,&data.counter,data.byteArray);
  longAddByte(2021824,&data.counter,data.byteArray);
  longAddByte(14564,&data.counter,data.byteArray);
  floatAddByte(0.49,&data.counter,data.byteArray);
  floatAddByte(-0.49,&data.counter,data.byteArray);
  floatAddByte(1.22,&data.counter,data.byteArray);
  floatAddByte(36.52,&data.counter,data.byteArray);
  floatAddByte(996.33,&data.counter,data.byteArray);
  floatAddByte(-0.46,&data.counter,data.byteArray);
  floatAddByte(-0.14,&data.counter,data.byteArray);
  floatAddByte(-9.66,&data.counter,data.byteArray);
  floatAddByte(-0.00,&data.counter,data.byteArray);
  floatAddByte(0.00,&data.counter,data.byteArray);
  floatAddByte(0.00,&data.counter,data.byteArray);
  floatAddByte(-25.69,&data.counter,data.byteArray);
  floatAddByte(-35.19,&data.counter,data.byteArray);
  floatAddByte(12.44,&data.counter,data.byteArray);
  floatAddByte(-179.00,&data.counter,data.byteArray);
  floatAddByte(2.93,&data.counter,data.byteArray);
  floatAddByte(-33.18,&data.counter,data.byteArray);

  longAddByte(-118334965,&data.counter,data.byteArray);
  longAddByte(95731051,&data.counter,data.byteArray);
  longAddByte(892087,&data.counter,data.byteArray);
  byteAddByte(0,&data.counter,data.byteArray);
  longAddByte(2021824,&data.counter,data.byteArray);
  longAddByte(14564,&data.counter,data.byteArray);
  state = radio.startTransmit(data.byteArray, 178);
  // you can transmit C-string or Arduino string up to
  // 256 characters long
  //transmissionState = radio.startTransmit("Hello World!");

  // you can also transmit byte array up to 256 bytes long
  /*
    byte byteArr[] = {0x01, 0x23, 0x45, 0x67,
                      0x89, 0xAB, 0xCD, 0xEF};
    state = radio.startTransmit(byteArr, 8);
  */
  
}

// flag to indicate that a packet was sent
volatile bool transmittedFlag = false;

// disable interrupt when it's not needed
volatile bool enableInterrupt = true;

// this function is called when a complete packet
// is transmitted by the module
// IMPORTANT: this function MUST be 'void' type
//            and MUST NOT have any arguments!
void setFlag(void) {
  // check if the interrupt is enabled
  if(!enableInterrupt) {
    return;
  }

  // we sent a packet, set the flag
  transmittedFlag = true;
}

void loop() {
  // check if the previous transmission finished
  if(transmittedFlag) {
    // disable the interrupt service routine while
    // processing the data
    enableInterrupt = false;

    // reset flag
    transmittedFlag = false;

    if (transmissionState == ERR_NONE) {
      // packet was successfully sent
      Serial.println(F("transmission finished!"));

      // NOTE: when using interrupt-driven transmit method,
      //       it is not possible to automatically measure
      //       transmission data rate using getDataRate()

    } else {
      Serial.print(F("failed, code "));
      Serial.println(transmissionState);

    }

    // wait a second before transmitting again
    //delay(1000);

    // send another one
    Serial.print(F("[SX1262] Sending another packet ... "));

    // you can transmit C-string or Arduino string up to
    // 256 characters long
    int start=millis();
    transmissionState =radio.startTransmit(data.byteArray, 178);
    int stop=millis();
    Serial.printf("tx time:%d\n",stop-start);    
    // you can also transmit byte array up to 256 bytes long
    /*
      byte byteArr[] = {0x01, 0x23, 0x45, 0x67,
                        0x89, 0xAB, 0xCD, 0xEF};
      int state = radio.startTransmit(byteArr, 8);
    */

    // we're ready to send more packets,
    // enable interrupt service routine
    enableInterrupt = true;
  }
  if (millis() - lastTime > 1000)
  {
    lastTime = millis(); //Update the timer

    long latitude = myGNSS.getLatitude();
    Serial.print(F("Lat: "));
    Serial.print(latitude);

    long longitude = myGNSS.getLongitude();
    Serial.print(F(" Long: "));
    Serial.print(longitude);
    Serial.print(F(" (degrees * 10^-7)"));

    long altitude = myGNSS.getAltitude();
    Serial.print(F(" Alt: "));
    Serial.print(altitude);
    Serial.print(F(" (mm)"));

    byte SIV = myGNSS.getSIV();
    Serial.print(F(" SIV: "));
    Serial.print(SIV);

    Serial.println();
    Serial.print(myGNSS.getYear());
    Serial.print("-");
    Serial.print(myGNSS.getMonth());
    Serial.print("-");
    Serial.print(myGNSS.getDay());
    Serial.print(" ");
    Serial.print(myGNSS.getHour());
    Serial.print(":");
    Serial.print(myGNSS.getMinute());
    Serial.print(":");
    Serial.print(myGNSS.getSecond());

    Serial.print("  Time is ");
    if (myGNSS.getTimeValid() == false)
    {
      Serial.print("not ");
    }
    Serial.print("valid  Date is ");
    if (myGNSS.getDateValid() == false)
    {
      Serial.print("not ");
    }
    Serial.print("valid");

    Serial.println();
  }
}
