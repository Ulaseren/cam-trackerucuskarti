#include <Arduino.h>
#include <LoRaWan-Arduino.h>
#include <SPI.h>

#define SCHED_MAX_EVENT_DATA_SIZE  APP_TIMER_SCHED_EVENT_DATA_SIZE /**< Maximum size of scheduler events. */
#define SCHED_QUEUE_SIZE 60                      /**< Maximum number of events in the scheduler queue. */


#define LORAWAN_APP_DATA_BUFF_SIZE 64  /**< Size of the data to be transmitted. */
#define LORAWAN_APP_INTERVAL 20000   /**< Defines the application data transmission duty cycle. 20s, value in [ms]. */

#define LORAWAN_DEFAULT_DATARATE DR_2                   /*LoRaMac datarates definition, from DR_0 to DR_5*/
#define LORAWAN_DEFAULT_TX_POWER TX_POWER_5  
#define JOINREQ_NBTRIALS 3 


DeviceClass_t CurrentClass = CLASS_A;          /* Class definition*/
LoRaMacRegion_t CurrentRegion = LORAMAC_REGION_EU868;    /* Region:EU868*/
lmh_confirm CurrentConfirm = LMH_UNCONFIRMED_MSG;   /* LMH_CONFIRMED_MSG olarak değiştirirsek node tarafına gatewayden durum mesajı alacağız buda ekstra mesaj trafik sağlayacak. */

hw_config hwConfig;

//#ifdef ESP32
// ESP32 - SX126x pin configuration
int PIN_LORA_RESET = 32;   // LORA RESET
int PIN_LORA_NSS = 15;   // LORA SPI CS
int PIN_LORA_SCLK = 14;  // LORA SPI CLK
int PIN_LORA_MISO = 12;  // LORA SPI MISO
int PIN_LORA_DIO_1 = 4; // LORA DIO_1
int PIN_LORA_BUSY = 33;  // LORA SPI BUSY
int PIN_LORA_MOSI = 13;  // LORA SPI MOSI
int RADIO_TXEN = 0;   // LORA ANTENNA TX ENABLE ,issues kısmında bu txen ve rxen kesinlikle tanımlı olması gerektiğini söylemişler yoksa asla başarılı olamayacağını bildirmiişler. 
int RADIO_RXEN = 2;   // LORA ANTENNA RX ENABLE ,Anten anahtarını kontrol etmeden gönderebilirsiniz,Join Accept  paketini asla alamazsınız.


// Foward declaration

static void lorawan_has_joined_handler(void);
static void lorawan_rx_handler(lmh_app_data_t *app_data);
static void lorawan_confirm_class_handler(DeviceClass_t Class);
static void lorawan_join_failed_handler(void);
static void send_lora_frame(void);
static uint32_t timers_init(void);
//void lmh_setSingleChannelGateway(uint8_t userSingleChannel, int8_t userDatarate);

// APP_TIMER_DEF(lora_tx_timer_id);                                            
TimerEvent_t appTimer;                              ///< LoRa tranfer timer instance.
static uint8_t m_lora_app_data_buffer[LORAWAN_APP_DATA_BUFF_SIZE];        ///< Lora user application data buffer.
static lmh_app_data_t m_lora_app_data = {m_lora_app_data_buffer, 0, 0, 0, 0}; ///< Lora user application data structure.

/**@brief Structure containing LoRaWan parameters, needed for lmh_init()
*/
static lmh_param_t lora_param_init = {LORAWAN_ADR_OFF,LORAWAN_DEFAULT_DATARATE, LORAWAN_PUBLIC_NETWORK,JOINREQ_NBTRIALS, LORAWAN_DEFAULT_TX_POWER , LORAWAN_DUTYCYCLE_ON};

/**@brief Structure containing LoRaWan callback functions, needed for lmh_init()
*/

static lmh_callback_t lora_callbacks = {BoardGetBatteryLevel, BoardGetUniqueId, BoardGetRandomSeed,
                    lorawan_rx_handler, lorawan_has_joined_handler, lorawan_confirm_class_handler, lorawan_join_failed_handler};


//#define LED_BUILTIN 0



uint8_t nodeDeviceEUI[8] = {0x60, 0x81, 0xF9, 0x3B, 0x7A, 0x46, 0xEA, 0xFF}; // msb 

uint8_t nodeAppEUI[8] = {0x60, 0x81, 0xF9, 0x86, 0x04, 0x39, 0x17, 0x32}; // msb

uint8_t nodeAppKey[16] = {0x30, 0x97, 0xD4, 0xAC, 0x4C, 0x8A, 0x8B, 0xE9, 0x6B, 0x69, 0x56, 0x29, 0x85, 0x3F, 0xFE, 0xBE}; //msb



void setup()
{ delay(5000);
//  pinMode(LED_BUILTIN, OUTPUT);
//  digitalWrite(LED_BUILTIN, LOW);

  // Define the HW configuration between MCU and SX126x
  hwConfig.CHIP_TYPE = SX1262_CHIP;     // Example uses an eByte E22 module with an SX1262
  hwConfig.PIN_LORA_RESET = PIN_LORA_RESET; // LORA RESET
  hwConfig.PIN_LORA_NSS = PIN_LORA_NSS;   // LORA SPI CS
  hwConfig.PIN_LORA_SCLK = PIN_LORA_SCLK;   // LORA SPI CLK
  hwConfig.PIN_LORA_MISO = PIN_LORA_MISO;   // LORA SPI MISO
  hwConfig.PIN_LORA_DIO_1 = PIN_LORA_DIO_1; // LORA DIO_1
  hwConfig.PIN_LORA_BUSY = PIN_LORA_BUSY;   // LORA SPI BUSY
  hwConfig.PIN_LORA_MOSI = PIN_LORA_MOSI;   // LORA SPI MOSI
  hwConfig.RADIO_TXEN = RADIO_TXEN;     // LORA ANTENNA TX ENABLE
  hwConfig.RADIO_RXEN = RADIO_RXEN;     // LORA ANTENNA RX ENABLE


  // Initialize Serial for debug output
  Serial.begin(115200);

  Serial.println("=====================================");
  Serial.println("SX126x LoRaWan test");
  Serial.println("=====================================");


  // Initialize Scheduler and timer
  uint32_t err_code = timers_init();
  if (err_code != 0)
  {
    Serial.printf("timers_init failed - %d\n", err_code);
  }


  err_code = lora_hardware_init(hwConfig);
  if (err_code != 0)
  {
    Serial.printf("lora_hardware_init failed - %d\n", err_code);
  }

  // Setup the EUIs and Keys
  lmh_setDevEui(nodeDeviceEUI);
  lmh_setAppEui(nodeAppEUI);
  lmh_setAppKey(nodeAppKey);



  err_code = lmh_init(&lora_callbacks, lora_param_init, true , CurrentClass,CurrentRegion); // 3. parametre true seçilirse OTAA aktif edilmiş olur. 

  if (err_code != 0)
  {
    Serial.printf("lmh_init failed - %d\n", err_code);
  }

  if (!lmh_setSubBandChannels(1))
  {
    Serial.println("lmh_setSubBandChannels failed. Wrong sub band requested?");
  }

  // Start Join procedure
  lmh_join();
}


void loop()
{
  
}

/**@brief LoRa function for handling OTAA join failed
*/
static void lorawan_join_failed_handler(void)
{
  Serial.println("OVER_THE_AIR_ACTIVATION failed!");
  Serial.println("Check your EUI's and Keys's!");
  Serial.println("Check if a Gateway is in range!");
}

/**@brief LoRa function for handling HasJoined event.
*/
static void lorawan_has_joined_handler(void)
{
#if (OVER_THE_AIR_ACTIVATION != 0)
  Serial.println("Network Joined");
#else
  Serial.println("OVER_THE_AIR_ACTIVATION != 0");

#endif
  lmh_class_request(CurrentClass); //Class değişimini buranın içine yazarak yapabiliriz.

  TimerSetValue(&appTimer, LORAWAN_APP_INTERVAL);
  TimerStart(&appTimer);
}

/**@brief Function for handling LoRaWan received data from Gateway
   @param[in] app_data  Pointer to rx data
*/
static void lorawan_rx_handler(lmh_app_data_t *app_data)
{
  Serial.printf("LoRa Packet received on port %d, size:%d, rssi:%d, snr:%d\n",
          app_data->port, app_data->buffsize, app_data->rssi, app_data->snr);

  switch (app_data->port)
  {
  case 3:
    // Port 3 switches the class
    if (app_data->buffsize == 1)
    {
      switch (app_data->buffer[0])
      {
      case 0:
        lmh_class_request(CLASS_A);
        break;

      case 1:
        lmh_class_request(CLASS_B);
        break;

      case 2:
        lmh_class_request(CLASS_C);
        break;

      default:
        break;
      }
    }
    break;

  case LORAWAN_APP_PORT:
    // YOUR_JOB: Take action on received data
    break;

  default:
    break;
  }
}

static void lorawan_confirm_class_handler(DeviceClass_t Class)
{
  Serial.printf("switch to class %c done\n", "ABC"[Class]);

  // Informs the server that switch has occurred ASAP
  m_lora_app_data.buffsize = 0;
  m_lora_app_data.port = LORAWAN_APP_PORT;
  lmh_send(&m_lora_app_data, CurrentConfirm);
}

static void send_lora_frame(void)
{
  if (lmh_join_status_get() != LMH_SET)
  {
    //Not joined, try again later
    Serial.println("Did not join network, skip sending frame");
    return;
  }

  uint32_t i = 0;
  m_lora_app_data.port = LORAWAN_APP_PORT;
  m_lora_app_data.buffer[i++] = 'H';
  m_lora_app_data.buffer[i++] = 'e';
  m_lora_app_data.buffer[i++] = 'l';
  m_lora_app_data.buffer[i++] = 'l';
  m_lora_app_data.buffer[i++] = 'o';
/*  m_lora_app_data.buffer[i++] = ' ';
  m_lora_app_data.buffer[i++] = 'w';
  m_lora_app_data.buffer[i++] = 'o';
  m_lora_app_data.buffer[i++] = 'r';
  m_lora_app_data.buffer[i++] = 'l';
  m_lora_app_data.buffer[i++] = 'd';
  m_lora_app_data.buffer[i++] = '!'; */
  m_lora_app_data.buffsize = i;

  lmh_error_status error = lmh_send(&m_lora_app_data, CurrentConfirm);
  Serial.printf("lmh_send result %d\n", error); // result 0  geliyorsa paket başarılı şekilde iletilmiş demektir. Eğer result -1 olursa transiever busy olduğu anlamına geliyor. 
}

/**@brief Function for handling a LoRa tx timer timeout event.
*/
static void tx_lora_periodic_handler(void)
{
  TimerSetValue(&appTimer, LORAWAN_APP_INTERVAL);
  TimerStart(&appTimer);
  Serial.println("Sending frame");
  send_lora_frame();
}



/**@brief Function for the Timer initialization.
   @details Initializes the timer module. This creates and starts application timers.
*/
static uint32_t timers_init(void)
{
  appTimer.timerNum = 3;
  TimerInit(&appTimer, tx_lora_periodic_handler);
  return 0;
}
