EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+3.3V #PWR050
U 1 1 6093CB48
P 8100 2450
F 0 "#PWR050" H 8100 2300 50  0001 C CNN
F 1 "+3.3V" H 8115 2623 50  0000 C CNN
F 2 "" H 8100 2450 50  0001 C CNN
F 3 "" H 8100 2450 50  0001 C CNN
	1    8100 2450
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR049
U 1 1 6093D8D8
P 4150 2150
F 0 "#PWR049" H 4150 2000 50  0001 C CNN
F 1 "+3.3V" H 4165 2323 50  0000 C CNN
F 2 "" H 4150 2150 50  0001 C CNN
F 3 "" H 4150 2150 50  0001 C CNN
	1    4150 2150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR054
U 1 1 6093DFCE
P 7150 5050
F 0 "#PWR054" H 7150 4800 50  0001 C CNN
F 1 "GND" H 7155 4877 50  0000 C CNN
F 2 "" H 7150 5050 50  0001 C CNN
F 3 "" H 7150 5050 50  0001 C CNN
	1    7150 5050
	1    0    0    -1  
$EndComp
$Comp
L 805_cap:Cap C20
U 1 1 6093F1B1
P 7700 2700
F 0 "C20" V 7704 2803 50  0000 L CNN
F 1 "22uF" V 7795 2803 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7700 2700 50  0001 L BNN
F 3 "" H 7700 2700 50  0001 L BNN
	1    7700 2700
	0    1    1    0   
$EndComp
$Comp
L 805_cap:Cap C21
U 1 1 6093FD89
P 8100 2700
F 0 "C21" V 8104 2803 50  0000 L CNN
F 1 "100nF" V 8195 2803 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8100 2700 50  0001 L BNN
F 3 "" H 8100 2700 50  0001 L BNN
	1    8100 2700
	0    1    1    0   
$EndComp
$Comp
L 805_cap:Cap C22
U 1 1 609405B2
P 4150 2850
F 0 "C22" V 4154 2953 50  0000 L CNN
F 1 "100nF" V 4245 2953 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4150 2850 50  0001 L BNN
F 3 "" H 4150 2850 50  0001 L BNN
	1    4150 2850
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R15
U 1 1 60941843
P 4150 2400
F 0 "R15" H 4218 2446 50  0000 L CNN
F 1 "10K" H 4218 2355 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4190 2390 50  0001 C CNN
F 3 "~" H 4150 2400 50  0001 C CNN
	1    4150 2400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR052
U 1 1 60942F63
P 4150 3150
F 0 "#PWR052" H 4150 2900 50  0001 C CNN
F 1 "GND" H 4155 2977 50  0000 C CNN
F 2 "" H 4150 3150 50  0001 C CNN
F 3 "" H 4150 3150 50  0001 C CNN
	1    4150 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR051
U 1 1 60946688
P 8100 3050
F 0 "#PWR051" H 8100 2800 50  0001 C CNN
F 1 "GND" H 8105 2877 50  0000 C CNN
F 2 "" H 8100 3050 50  0001 C CNN
F 3 "" H 8100 3050 50  0001 C CNN
	1    8100 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 2900 7700 2950
Wire Wire Line
	7700 2950 8100 2950
Wire Wire Line
	8100 2950 8100 2900
Wire Wire Line
	8100 3050 8100 2950
Connection ~ 8100 2950
Wire Wire Line
	7000 5000 7150 5000
Wire Wire Line
	7150 5000 7150 5050
Wire Wire Line
	7150 4600 7000 4600
Wire Wire Line
	7150 4500 7000 4500
Wire Wire Line
	7150 4400 7000 4400
Wire Wire Line
	7150 4300 7000 4300
Wire Wire Line
	7150 4200 7000 4200
Wire Wire Line
	7150 4100 7000 4100
Wire Wire Line
	7150 4000 7000 4000
Wire Wire Line
	7150 3900 7000 3900
Wire Wire Line
	7150 3800 7000 3800
Wire Wire Line
	7150 3700 7000 3700
Wire Wire Line
	7150 2900 7000 2900
Wire Wire Line
	7150 3000 7000 3000
Wire Wire Line
	7150 3100 7000 3100
Wire Wire Line
	7150 3200 7000 3200
Wire Wire Line
	7150 3300 7000 3300
Wire Wire Line
	7150 3400 7000 3400
Wire Wire Line
	7150 3500 7000 3500
Wire Wire Line
	7150 3600 7000 3600
Wire Wire Line
	5150 3000 5050 3000
Wire Wire Line
	5150 3100 5050 3100
Wire Wire Line
	5150 3350 5050 3350
Wire Wire Line
	5150 3450 5050 3450
Wire Wire Line
	5150 3700 5050 3700
Wire Wire Line
	5150 3800 5050 3800
Wire Wire Line
	4150 3150 4150 3050
Wire Wire Line
	4150 2550 4150 2600
Connection ~ 4150 2650
Wire Wire Line
	4150 2650 4150 2750
Wire Wire Line
	4150 2600 4050 2600
Connection ~ 4150 2600
Wire Wire Line
	4150 2600 4150 2650
Wire Wire Line
	4150 2250 4150 2150
$Comp
L Device:R_US R16
U 1 1 60957214
P 8850 3500
F 0 "R16" H 8918 3546 50  0000 L CNN
F 1 "10K" H 8918 3455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8890 3490 50  0001 C CNN
F 3 "~" H 8850 3500 50  0001 C CNN
	1    8850 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R17
U 1 1 60957FEA
P 9200 3500
F 0 "R17" H 9268 3546 50  0000 L CNN
F 1 "10K" H 9268 3455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9240 3490 50  0001 C CNN
F 3 "~" H 9200 3500 50  0001 C CNN
	1    9200 3500
	1    0    0    -1  
$EndComp
Text Label 8600 3700 0    50   ~ 0
SCL
Text Label 8600 3850 0    50   ~ 0
SDA
Text Label 7000 4400 0    50   ~ 0
SCL
Text Label 7000 4300 0    50   ~ 0
SDA
$Comp
L power:+3.3V #PWR053
U 1 1 6095D268
P 9050 3200
F 0 "#PWR053" H 9050 3050 50  0001 C CNN
F 1 "+3.3V" H 9065 3373 50  0000 C CNN
F 2 "" H 9050 3200 50  0001 C CNN
F 3 "" H 9050 3200 50  0001 C CNN
	1    9050 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 3200 9050 3350
Wire Wire Line
	9050 3350 8850 3350
Wire Wire Line
	9050 3350 9200 3350
Connection ~ 9050 3350
Wire Wire Line
	8850 3650 8850 3700
Wire Wire Line
	8850 3700 8600 3700
Wire Wire Line
	8600 3850 9200 3850
Wire Wire Line
	9200 3850 9200 3650
Text HLabel 4050 2600 0    50   Input ~ 0
EN
Text HLabel 5050 3000 0    50   Input ~ 0
SENSOR_VP
Wire Wire Line
	4150 2650 5150 2650
Text HLabel 5050 3100 0    50   Input ~ 0
SENSOR_VN
Text HLabel 5050 3350 0    50   Input ~ 0
IO34
Text HLabel 5050 3450 0    50   Input ~ 0
IO35
Text HLabel 7150 2900 2    50   Input ~ 0
IO0
Text HLabel 7150 3000 2    50   Input ~ 0
IO2
Text HLabel 7150 3100 2    50   Input ~ 0
IO4
Text HLabel 7150 3200 2    50   Input ~ 0
IO5
Text HLabel 7150 3300 2    50   Input ~ 0
IO12
Text HLabel 7150 3400 2    50   Input ~ 0
IO13
Text HLabel 7150 3500 2    50   Input ~ 0
IO14
Text HLabel 7150 3600 2    50   Input ~ 0
IO15
Text HLabel 7150 3700 2    50   Input ~ 0
IO18
Text HLabel 7150 3800 2    50   Input ~ 0
IO19
Text HLabel 7150 3900 2    50   Input ~ 0
IO21
Text HLabel 7150 4000 2    50   Input ~ 0
IO22
Text HLabel 7150 4100 2    50   Input ~ 0
IO23
Text HLabel 7150 4200 2    50   Input ~ 0
IO25
Text HLabel 7150 4300 2    50   Input ~ 0
IO26
Text HLabel 7150 4400 2    50   Input ~ 0
IO27
Text HLabel 7150 4500 2    50   Input ~ 0
IO32
Text HLabel 7150 4600 2    50   Input ~ 0
IO33
$Comp
L ESP32-WROVER-B__16MB_:ESP32-WROVER-B_(16MB) U7
U 1 1 6119AECC
P 6050 3800
F 0 "U7" H 6075 5267 50  0000 C CNN
F 1 "ESP32-WROVER-B_(16MB)" H 6075 5176 50  0000 C CNN
F 2 "others:ESP32-WROVER-B_(16MB)" H 5400 2750 50  0001 L BNN
F 3 "" H 6050 3800 50  0001 L BNN
F 4 "Manufacturer Recommendations" H 5400 2750 50  0001 L BNN "STANDARD"
F 5 "ESPRESSIF SYSTEMS" H 5700 2950 50  0001 L BNN "MANUFACTURER"
F 6 "1.1" H 6050 3800 50  0001 L BNN "PARTREV"
	1    6050 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 2600 7700 2600
Wire Wire Line
	7700 2600 8100 2600
Connection ~ 7700 2600
Wire Wire Line
	8100 2600 8100 2450
Connection ~ 8100 2600
Text HLabel 5050 3800 0    50   Input ~ 0
RXD0
Text HLabel 5050 3700 0    50   Input ~ 0
TXD0
$EndSCHEMATC
